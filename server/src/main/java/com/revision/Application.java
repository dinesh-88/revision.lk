package com.revision;

import com.revision.admin.dao.MySqlDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;
import javax.validation.Valid;
import java.lang.management.ManagementFactory;

@SpringBootApplication
public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    @Autowired
    DataSource dataSource;

    @Autowired
    MySqlDaoImpl mySqlDao;

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
        logger.info("######(Application)-----------: Application Started.   pid:" + getPid() + "  ###### " );

    }

    private static String getPid(){
        final String jvmName = ManagementFactory.getRuntimeMXBean().getName();
        return jvmName.substring(0,jvmName.indexOf("@"));
    }
}
