package com.revision;

import com.google.gson.Gson;
import com.revision.admin.bo.*;
import com.revision.admin.utils.AdminMeta;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Gson gson = new Gson();
       /* AdminMessageHeader adminMessageHeader = new AdminMessageHeader();
        adminMessageHeader.setMsgType(AdminMeta.GET_SUBJECT_LIST);
        adminMessageHeader.setUserID("Mano");
        adminMessageHeader.setIp("127.0.0.1");
        adminMessageHeader.setMessageID("1314564");

        AdminMessage adminMessage = new AdminMessage();
        adminMessage.setHeader(adminMessageHeader);
        System.out.println(gson.toJson(adminMessage));

        AdminMessageHeader adminMessageHeader1 = new AdminMessageHeader();
        adminMessageHeader1.setMsgType(AdminMeta.GET_SECTIONS_FOR_SUBJECT);
        adminMessageHeader1.setUserID("Mano");
        adminMessageHeader1.setIp("127.0.0.1");
        adminMessageHeader1.setMessageID("1314564");

        Subject subject = new Subject();
        subject.setSubjectCode("01");

        AdminMessage adminMessage1 = new AdminMessage();
        adminMessage1.setHeader(adminMessageHeader1);
        adminMessage1.setBody(subject);

        System.out.println(gson.toJson(adminMessage1));*/

       /* AdminMessageHeader adminMessageHeader1 = new AdminMessageHeader();
        adminMessageHeader1.setMsgType(AdminMeta.INSERT_PAPER_DETAIL);
        adminMessageHeader1.setUserID("Mano");
        adminMessageHeader1.setIp("127.0.0.1");
        adminMessageHeader1.setMessageID("1314564");

        PaperDetail paperDetail = new PaperDetail();
        paperDetail.setPaperId("1");
        paperDetail.setQuestionId("2");
        paperDetail.setQuestion("ප්ලාන්ක් නියතයෙහි මාන වලට සමාන මාන ඇති රාශිය වන්නේ");
        paperDetail.setAnswer("2");
        paperDetail.setSectionCode("01_1");
        paperDetail.setSubSectionCode("01_1_1");
        paperDetail.setDescription("Solution Description Here");
        paperDetail.setChoise1("ශක්තිය");
        paperDetail.setChoise2("ක්ෂමතාවය");
        paperDetail.setChoise3("කෝණික ස0ඛ්යාතය");
        paperDetail.setChoise4("ව්\u200Dයාවර්තය");
        paperDetail.setChoise5("කෝණික ගම්\u200Dයතාව");*/


        AdminMessageHeader adminMessageHeader1 = new AdminMessageHeader();
        adminMessageHeader1.setMsgType(AdminMeta.CREATE_NEW_USER);
        adminMessageHeader1.setUserID("Mano");
        adminMessageHeader1.setIp("127.0.0.1");
        adminMessageHeader1.setMessageID("1314564");

        User user = new User();
        user.setUserName("ManoPakaya");
        user.setPassword("123");
        user.setEmail("maano@gmssil.com");
        user.setMobile("+9471223445");
        user.setFirstName("Manodya");
        user.setLastName("Lakmal");

        AdminMessage adminMessage = new AdminMessage();
        adminMessage.setHeader(adminMessageHeader1);
        adminMessage.setBody(user);
        System.out.println(gson.toJson(adminMessage));





    }
}
