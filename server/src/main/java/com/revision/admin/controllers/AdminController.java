package com.revision.admin.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.revision.admin.bo.*;
import com.revision.admin.dao.MySqlDaoImpl;
import com.revision.admin.utils.AdminMeta;
import com.revision.admin.utils.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jws.soap.SOAPBinding;
import javax.sql.DataSource;
import javax.validation.Valid;
import javax.ws.rs.POST;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@RestController
@CrossOrigin()
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class AdminController {
    private static final Logger logger = LogManager.getLogger(AdminController.class);
    private static Gson gson = new Gson();
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static String UPLOADED_FOLDER = "G://temp//";

    @Autowired
    DataSource dataSource;
    @Autowired
    MySqlDaoImpl mySqlDao;

    @RequestMapping(value = "/admin/", method = {RequestMethod.POST}, headers = {"Content-type=application/json"})
    @ResponseBody
    public String handleAdminRequest(@RequestBody @Valid AdminMessage adminMessage) {
        String response = null;
        AdminMessage responseMessage = new AdminMessage();
        logger.info("######(Admin)-----------: Request Received:" + gson.toJson(adminMessage));
        switch (adminMessage.getHeader().getMsgType()) {
            case AdminMeta.GET_SUBJECT_LIST:
                response = gson.toJson(processGetSubjectList(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.GET_SECTIONS_FOR_SUBJECT:
                response = gson.toJson(processGetSectionsForSubject(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.GET_SUB_SECTIONS_FOR_SECTION:
                response = gson.toJson(processGetSubSectionsForSection(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.GET_ADMIN_USER_PAPERS:
                response = gson.toJson(processGetAdminUserPapers(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.GET_PAPER_DETAILS:
                response = gson.toJson(processGetPaperDetails(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.INSERT_PAPER_META:
                response = gson.toJson(processInsertPaperMeta(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.INSERT_PAPER_DETAIL:
                response = gson.toJson(processInsertPaperDetail(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.CREATE_NEW_USER:
                response = gson.toJson(processCreateNewUser(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.UPDATE_COMPLETE_PAPER:
                response = gson.toJson(processCompletePaper(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.UPDATE_REVIEW_PAPER:
                response = gson.toJson(processReviewPaper(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;
            case AdminMeta.ADMIN_LOGIN:
                response = gson.toJson(processAdminLogin(adminMessage));
                logger.info("######(Admin)-----------: Response  Sent:" + gson.toJson(response));
                return response;

        }
        return null;
    }


    @RequestMapping(value = "/upload/{paperID}/{questionID}", method = {RequestMethod.POST}, headers = {"Content-type=multipart/form-data"})
    @CrossOrigin()
    public  String fileUpload(@RequestParam("file") MultipartFile file,
                              @PathVariable String paperID,
                              @PathVariable String questionID,
                              RedirectAttributes redirectAttributes){

        try {
            System.out.println("Paper ID:" + paperID + " QuestionID:" + questionID);
            System.out.println(file.getOriginalFilename());
            byte[] bytes = file.getBytes();
            String filePath = UPLOADED_FOLDER + paperID + "_" + questionID + "_"+ file.getOriginalFilename().trim();
            Path path = Paths.get(filePath);
            Files.write(path, bytes);
            mySqlDao.updateQuestionImage(filePath, Integer.parseInt(paperID), Integer.parseInt(questionID));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Done";

    }


    public Object processGetSubjectList(AdminMessage adminMessage) {
        try {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.getSubjectList());
        } catch (Exception e) {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        } finally {
            return adminMessage;
        }

    }

    public Object processGetSectionsForSubject(AdminMessage adminMessage) {
        try {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.getSectionsForSubject(objectMapper.convertValue(adminMessage.getBody(), Subject.class).getSubjectCode()));
        } catch (Exception e) {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        } finally {
            return adminMessage;
        }

    }

    public Object processGetSubSectionsForSection(AdminMessage adminMessage) {
        try {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.getSubsectionsForSection(objectMapper.convertValue(adminMessage.getBody(), Section.class).getSectionCode()));
        } catch (Exception e) {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        } finally {
            return adminMessage;
        }

    }

    public Object processGetAdminUserPapers(AdminMessage adminMessage){

        try {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.getAdminUserPapers(objectMapper.convertValue(adminMessage.getBody(), Paper.class).getUsername()));
        } catch (Exception e) {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        } finally {
            return adminMessage;
        }
    }

    public Object processGetPaperDetails(AdminMessage adminMessage){

        try {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.getPaperDetails(objectMapper.convertValue(adminMessage.getBody(), Paper.class).getPaperId()));
        } catch (Exception e) {
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        } finally {
            return adminMessage;
        }
    }

    public Object processInsertPaperMeta(AdminMessage adminMessage) {
        try{
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.insertPaperMeta(objectMapper.convertValue(adminMessage.getBody(), Paper.class)));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        }finally {
            return adminMessage;
        }
    }

    public Object processInsertPaperDetail(AdminMessage adminMessage) {
        try{
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.insertPaperDetail(objectMapper.convertValue(adminMessage.getBody(), PaperDetail.class)));
          //  mySqlDao.insertPaperDetail(objectMapper.convertValue(adminMessage.getBody(), PaperDetail.class));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        }finally {
            return adminMessage;
        }
    }

    public Object processCompletePaper(AdminMessage adminMessage) {
        try{
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.updateCompletePaper(objectMapper.convertValue(adminMessage.getBody(), Paper.class)));
           // mySqlDao.updateCompletePaper(objectMapper.convertValue(adminMessage.getBody(), Paper.class));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        }finally {
            return adminMessage;
        }
    }

    public Object processReviewPaper(AdminMessage adminMessage) {
        try{
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.updateReviewPaper(objectMapper.convertValue(adminMessage.getBody(), Paper.class)));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        }finally {
            return adminMessage;
        }
    }

    public Object processAdminLogin(AdminMessage adminMessage) {
        AdminMessage responseMessage = new AdminMessage();
        try{
            responseMessage  = (AdminMessage) mySqlDao.adminUserLogin(objectMapper.convertValue(adminMessage.getBody(), User.class));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            logger.error("######(Admin-Error)-----------:" + e.getMessage());
        }finally {
            return responseMessage;
        }
    }

    public Object processCreateNewUser(AdminMessage adminMessage){
        try{
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            adminMessage.setBody(mySqlDao.createNewUser(objectMapper.convertValue(adminMessage.getBody(), User.class)));
        }catch (Exception e){
            adminMessage.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            if(e instanceof DuplicateKeyException){
                adminMessage.getHeader().setErrorMessage(Constants.USER_ALREADY_EXISTS);
            }else{
                adminMessage.getHeader().setErrorMessage(Constants.COMMON_ERROR);
            }

            adminMessage.setBody(null);
            logger.error("######(Admin-Error)-----------:" + e);
        }finally {
            return adminMessage;
        }
    }





}
