package com.revision.admin.dao;

import com.revision.admin.Customer;
import com.revision.admin.bo.*;

import java.util.List;

/**
 * Created by manodyas on 11/14/2017.
 */
public interface DaoI {
    public List<Customer> findAllCustomers() throws Exception;
    public List<Subject> getSubjectList() throws Exception;
    public List<Section> getSectionsForSubject(String subjectCode) throws Exception;
    public List<Subsection> getSubsectionsForSection(String sectionCode) throws Exception;
    public List<Paper> getAdminUserPapers(String userName) throws Exception;
    public List<PaperDetail> getPaperDetails(int paperId) throws Exception;
    public List<Packages> getDefaultPackage() throws Exception;
    public List<Packages> getPackageList() throws Exception;
    public Object insertPaperMeta(Paper paper) throws Exception;
    public Object insertPaperDetail(PaperDetail paperDetail) throws Exception;
    public Object createNewUser(User user) throws Exception;
    public Object updateCompletePaper(Paper paper) throws Exception;
    public Object updateReviewPaper(Paper paper) throws Exception;
    public Object updateQuestionImage(String imageUrl, int paperId, int questionId) throws Exception;
    public Object adminUserLogin(User user);
}

