package com.revision.admin.dao;

import com.revision.admin.Customer;
import com.revision.admin.bo.*;
import com.revision.admin.utils.AdminMeta;
import com.revision.admin.utils.Constants;
import com.revision.admin.utils.UtilHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.rmi.CORBA.Util;
import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

/**
 * Created by manodyas on 11/14/2017.
 */
@Repository
public class MySqlDaoImpl implements DaoI {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*--Test-*/
    @Override
    public List<Customer> findAllCustomers() throws Exception {
        List<Customer> result = jdbcTemplate.query(
                "SELECT id, name, email, created_date FROM customer",
                (rs, rowNum) -> new Customer(rs.getInt("id"),
                        rs.getString("name"), rs.getString("email"), rs.getDate("created_date"))
        );

        return result;
    }

    /*----*/

    @Override
    public List<Subject> getSubjectList() throws Exception {
        String sql = "select subject_code, description,created_date from subject";
        List<Subject> result = jdbcTemplate.query(sql,
                (rs, rowNum) -> new Subject(rs.getString("subject_code"),
                        rs.getString("description"),
                        rs.getDate("created_date")));
        return result;
    }

    @Override
    public List<Section> getSectionsForSubject(String subjectCode) throws Exception {
        String sql = "select subject_code, section_code,description,created_date from section where subject_code= ?";
        List<Section> result = jdbcTemplate.query(sql, new Object[]{subjectCode},
                (rs, rowNum) -> new Section(rs.getString("subject_code"),
                        rs.getString("section_code"),
                        rs.getString("description"),
                        rs.getDate("created_date")));
        return result;
    }

    @Override
    public List<Subsection> getSubsectionsForSection(String sectionCode) throws Exception {
        String sql = "select subject_code, section_code, subsection_code, description, created_date from subsection where section_code=?";
        List<Subsection> result = jdbcTemplate.query(sql, new Object[]{sectionCode},
                (rs, rowNum) -> new Subsection(rs.getString("subject_code"),
                        rs.getString("section_code"),
                        rs.getString("subsection_code"),
                        rs.getString("description"),
                        rs.getDate("created_date")));
        return result;
    }

    public List<Paper> getAdminUserPapers(String userName) throws Exception {
        String sql = "select * from paper where user_name= BINARY ? ";
        List<Paper> result = jdbcTemplate.query(sql, new Object[]{userName},
                (rs, rowNum) -> new Paper(rs.getInt("id"),
                        rs.getString("subject_code"),
                        rs.getString("description"),
                        rs.getDate("uploaded_date"),
                        rs.getString("user_name"),
                        rs.getDate("expiry_date"),
                        rs.getString("paper_status"),
                        AdminMeta.getStatusDescription(rs.getString("paper_status")),
                        rs.getString("paper_duration")
                        ));
        return result;
    }

    @Override
    public List<PaperDetail> getPaperDetails(int paperId) throws Exception {
        String sql = "select * from paper_details where paper_id=?";
        List<PaperDetail> result = jdbcTemplate.query(sql, new Object[]{paperId},
                (rs, rowNum) -> new PaperDetail(rs.getString("paper_id"),
                        rs.getString("question_id"),
                        rs.getString("question"),
                        rs.getString("answer"),
                        rs.getString("section_code"),
                        rs.getString("subsection_code"),
                        rs.getString("description"),
                        rs.getString("image_url"),
                        rs.getString("choise_1"),
                        rs.getString("choise_2"),
                        rs.getString("choise_3"),
                        rs.getString("choise_4"),
                        rs.getString("choise_5"),
                        rs.getDate("uploaded_date")));
        return result;
    }

    @Override
    public List<Packages> getDefaultPackage() throws Exception {
        String sql = "select * from packages where isDefault=true";

        List<Packages> result = jdbcTemplate.query(sql,
                (rs, rowNum) -> new Packages(rs.getInt("id"),
                        rs.getString("package_name"),
                        rs.getInt("paper_tokens"),
                        rs.getInt("paper_count"),
                        rs.getDouble("cost"),
                        rs.getInt("duration"),
                        rs.getBoolean("isDefault")));


        return result;
    }

    @Override
    public List<Packages> getPackageList() throws Exception {
        String sql = "select * from packages";

        List<Packages> result = jdbcTemplate.query(sql,
                (rs, rowNum) -> new Packages(rs.getInt("id"),
                        rs.getString("package_name"),
                        rs.getInt("paper_tokens"),
                        rs.getInt("paper_count"),
                        rs.getDouble("cost"),
                        rs.getInt("duration"),
                        rs.getBoolean("isDefault")));


        return result;
    }

    @Override
    public Object insertPaperMeta(Paper paper) throws Exception {

        String sql = "INSERT INTO `revision`.`paper`(`subject_code`,`description`,`user_name`,`paper_status`, `paper_duration`)" +
                "VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql, paper.getSubjectCode(),
                paper.getDescription(),
                paper.getUsername(),
                AdminMeta.PAPER_STATUS_INITIALIZED,
                paper.getPaperDuration()
        );

        sql = "SELECT MAX(id) FROM paper";
        List<Paper> result = jdbcTemplate.query(sql, new Object[]{}, (rs, rowNum) -> new Paper(rs.getInt("MAX(id)"))
        );
        return result.get(0);
    }

    @Override
    public Object insertPaperDetail(PaperDetail paperDetail) throws Exception {
        String sql = "INSERT INTO `revision`.`paper_details`(`paper_id`,`question_id`,`question`,`answer`,`section_code`,`subsection_code`,`description`,`image_url`,`choise_1`,`choise_2`,`choise_3`,`choise_4`,`choise_5`)" +
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, paperDetail.getPaperId(),
                paperDetail.getQuestionId(),
                paperDetail.getQuestion(),
                paperDetail.getAnswer(),
                paperDetail.getSectionCode(),
                paperDetail.getSubSectionCode(),
                paperDetail.getDescription(),
                paperDetail.getImageUrl(),
                paperDetail.getChoise1(),
                paperDetail.getChoise2(),
                paperDetail.getChoise3(),
                paperDetail.getChoise4(),
                paperDetail.getChoise5()
        );

        sql = "update paper set paper_status=? where id=?";

        jdbcTemplate.update(sql, AdminMeta.PAPER_STATUS_PARTIALLY_COMPOSED, paperDetail.getPaperId());


      /*  if (paperDetail.getQuestionId().equalsIgnoreCase(Constants.LAST_QUESTION_NUMBER)) {
            jdbcTemplate.update(sql, AdminMeta.PAPER_STATUS_FULLY_COMPOSED, paperDetail.getPaperId());
        } else {

        }*/


        return "OK";
    }

    @Override
    public Object createNewUser(User user) throws Exception {
        String sql = "INSERT INTO `revision`.`user`(`user_name`,`password`,`email`,`mobile`," +
                "`first_name`,`last_name`," +
                "`status`)VALUES(?,?,?,?,?,?,?)";

        jdbcTemplate.update(sql, user.getUserName(),
                UtilHelper.getEncryptedPassword(user.getPassword()),
                user.getEmail(),
                user.getMobile(),
                user.getFirstName(),
                user.getLastName(),
                AdminMeta.USER_STATUS_PENDING_APPROVE);
         List<Packages> packagesList = this.getDefaultPackage();
         if(packagesList != null &&  packagesList.size() > 0){

             String updateSql = "update user set package_id= ? where email=?";
             jdbcTemplate.update(updateSql, packagesList.get(0).getId(), user.getEmail());

         }

        return "OK";
    }

    @Override
    public Object updateCompletePaper(Paper paper) throws Exception {
        String sql = "update paper set paper_status=? , completed_date = ? where id=?;";
        jdbcTemplate.update(sql, AdminMeta.PAPER_STATUS_FULLY_COMPOSED, new Date(System.currentTimeMillis()), paper.getPaperId());
        return null;
    }

    @Override
    public Object updateReviewPaper(Paper paper) throws Exception {
        String sql = "update paper set paper_status=? , reviewed_date = ? where id=?;";
        jdbcTemplate.update(sql, AdminMeta.PAPER_STATUS_APPROVED, new Date(System.currentTimeMillis()), paper.getPaperId());
        return null;
    }

    @Override
    public Object updateQuestionImage(String imageUrl, int paperId, int questionId) throws Exception {
        String sql = "update paper_details set image_url =? where paper_id=? and question_id=?;";
        jdbcTemplate.update(sql, imageUrl, paperId, questionId);
        return null;
    }

    @Override
    public Object adminUserLogin(User user){
        AdminMessage response = new AdminMessage();
         AdminMessageHeader header = new AdminMessageHeader();
         response.setHeader(header);

        String sql = "select * from user where email=? and password=?";

        List<User> result = jdbcTemplate.query(sql,new Object[]{user.getEmail(), UtilHelper.getEncryptedPassword(user.getPassword())},
                (rs, rowNum) -> {
                    return new User(rs.getInt("id"),
                            rs.getString("user_name"),
                            rs.getString("email"),
                            rs.getString("mobile"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getDate("created_date"),
                            rs.getDate("last_login_time"),
                            rs.getDate("subscription_end_date"),
                            rs.getInt("status"),
                            rs.getInt("role_id"),
                            rs.getInt("package_id")
                            );
                });

        if(result != null && result.size() > 0){
            user = result.get(0);
            String updateLastLoginTimeSql = "update user set last_login_time=?  where id=?;";
            jdbcTemplate.update(updateLastLoginTimeSql,new Date(System.currentTimeMillis()), user.getId());

            String deleteSessionEntry = "delete from admin_session where user_id=?;";
            jdbcTemplate.update(deleteSessionEntry, user.getId());

            String insertSessionEntry = "INSERT INTO `revision`.`admin_session` (`user_id`,`session_id`,`last_activity_time`)" +
                    "VALUES(?,?, ?)";

            String sessionID =  UtilHelper.getSessionID();
            jdbcTemplate.update(insertSessionEntry, user.getId(), UtilHelper.getSessionID(), new Date(System.currentTimeMillis()));

            response.getHeader().setStatusCode(Constants.RESPONSE_SUCCESS);
            response.getHeader().setSessionID(sessionID);
            response.setBody(user);
        }else{
            response.getHeader().setStatusCode(Constants.RESPONSE_FAIL);
            response.getHeader().setErrorMessage(Constants.INVALID_USER_CREDENTIALS);
        }
        return response;
    }


}
