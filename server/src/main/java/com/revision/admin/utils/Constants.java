package com.revision.admin.utils;

/**
 * Created by manodyas on 11/16/2017.
 */
public class Constants {
    /*----Response Codes----*/
    public static final int RESPONSE_SUCCESS  = 1;
    public static final int RESPONSE_FAIL     = -1;

    /*----Response Messages---*/
    public static final String COMMON_ERROR = "Back End Error..!";
    public static final String USER_ALREADY_EXISTS = "User Already Exists...!!";
    public static final String INVALID_USER_CREDENTIALS = "Invalid User Credentials...!!";


    /*---Paper Details---*/
    public static final String LAST_QUESTION_NUMBER = "60";
}
