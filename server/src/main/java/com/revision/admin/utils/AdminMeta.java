package com.revision.admin.utils;

/**
 * Created by manodyas on 11/14/2017.
 */
public class AdminMeta {
    public static final String   GET_SUBJECT_LIST               	    = "1000";
    public static final String   GET_SECTIONS_FOR_SUBJECT               = "1001";
    public static final String   GET_SUB_SECTIONS_FOR_SECTION           = "1002";
    public static final String   GET_ADMIN_USER_PAPERS                  = "1003";
    public static final String   GET_PAPER_DETAILS                      = "1004";


    public static final String   INSERT_PAPER_META                      = "2000";
    public static final String   INSERT_PAPER_DETAIL                    = "2001";
    public static final String   CREATE_NEW_USER                        = "2002";

    public static final String   UPDATE_COMPLETE_PAPER                  = "3000";
    public static final String   UPDATE_REVIEW_PAPER                    = "3001";


    public static final String   ADMIN_LOGIN                    = "4000";











    /*-- Paper Status--*/
    public static final String PAPER_STATUS_INITIALIZED = "1";
    public static final String PAPER_STATUS_PARTIALLY_COMPOSED = "2";
    public static final String PAPER_STATUS_FULLY_COMPOSED = "3";
    public static final String PAPER_STATUS_APPROVED = "4";
    public static final String PAPER_STATUS_EXPIRED = "5";
    public static final String PAPER_STATUS_AMENDED = "6";

    public static final String PAPER_STATUS_INITIALIZED_DES = "INITIALIZED";
    public static final String PAPER_STATUS_PARTIALLY_COMPOSED_DES = "PARTIALLY_COMPOSED";
    public static final String PAPER_STATUS_FULLY_COMPOSED_DES = "FULLY_COMPOSED_PENDING_REVIEW";
    public static final String PAPER_STATUS_APPROVED_DES = "REVIEWED";
    public static final String PAPER_STATUS_EXPIRED_DES = "EXPIRED";
    public static final String PAPER_STATUS_AMENDED_DES = "AMENDED_PENDING_REVIEW";

    public static String getStatusDescription(String paperStatus) {
        switch (paperStatus) {
            case PAPER_STATUS_INITIALIZED:
                return PAPER_STATUS_INITIALIZED_DES;
            case PAPER_STATUS_PARTIALLY_COMPOSED:
                return PAPER_STATUS_PARTIALLY_COMPOSED_DES;
            case PAPER_STATUS_FULLY_COMPOSED:
                return PAPER_STATUS_FULLY_COMPOSED_DES;
            case PAPER_STATUS_APPROVED:
                return PAPER_STATUS_APPROVED_DES;
            case PAPER_STATUS_EXPIRED:
                return PAPER_STATUS_EXPIRED_DES;
            case PAPER_STATUS_AMENDED:
                return PAPER_STATUS_AMENDED_DES;

        }
        return null;
    }
    /*--------*/


    /*-----User Status----*/
    public static final int USER_STATUS_PENDING_APPROVE = 0;
    public static final int USER_STATUS_ACTIVE = 1;
}
