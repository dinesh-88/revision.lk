package com.revision.admin;

import java.util.Date;

/**
 * Created by manodyas on 11/14/2017.
 */
public class Customer {
    int id;
    String name;
    String email;
    Date date;

    public Customer(int id, String name, String email, Date date) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.date = date;
    }

}
