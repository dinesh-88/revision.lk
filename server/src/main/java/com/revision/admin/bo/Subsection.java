package com.revision.admin.bo;

import java.sql.Date;

/**
 * Created by manodyas on 11/17/2017.
 */
public class Subsection {
    private String subjectCode;
    private String sectionCode;
    private String subsectionCode;
    private String description;
    private Date createdDate;

    public Subsection(String subjectCode, String sectionCode, String subsectionCode, String description, Date createdDate) {
        this.subjectCode = subjectCode;
        this.sectionCode = sectionCode;
        this.subsectionCode = subsectionCode;
        this.description = description;
        this.createdDate = createdDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getSubsectionCode() {
        return subsectionCode;
    }

    public void setSubsectionCode(String subsectionCode) {
        this.subsectionCode = subsectionCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
