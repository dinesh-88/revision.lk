package com.revision.admin.bo;

import java.sql.Date;

/**
 * Created by manodyas on 11/14/2017.
 */
public class Subject extends AdminMessage {
    private String subjectCode;
    private String description;
    private Date createdDate;

    public Subject() {
    }

    public Subject(String subjectCode, String description, Date createdDate) {
        this.subjectCode = subjectCode;
        this.description = description;
        this.createdDate = createdDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
