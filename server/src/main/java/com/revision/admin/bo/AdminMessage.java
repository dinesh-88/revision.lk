package com.revision.admin.bo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by manodyas on 11/14/2017.
 */
public class AdminMessage {
   private AdminMessageHeader header;
   @JsonProperty
   private Object body;

    public AdminMessageHeader getHeader() {
        return header;
    }

    public void setHeader(AdminMessageHeader header) {
        this.header = header;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
