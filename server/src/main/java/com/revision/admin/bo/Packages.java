package com.revision.admin.bo;

/**
 * Created by manodyas on 11/28/2017.
 */
public class Packages {
    private int id;
    private String packageName;
    private int paperTokens;
    private int paperCount;
    private double cost;
    private int duration;
    private boolean isDefault;

    public Packages(int id, String packageName, int paperTokens, int paperCount, double cost, int duration, boolean isDefault) {
        this.id = id;
        this.packageName = packageName;
        this.paperTokens = paperTokens;
        this.paperCount = paperCount;
        this.cost = cost;
        this.duration = duration;
        this.isDefault = isDefault;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getPaperTokens() {
        return paperTokens;
    }

    public void setPaperTokens(int paperTokens) {
        this.paperTokens = paperTokens;
    }

    public int getPaperCount() {
        return paperCount;
    }

    public void setPaperCount(int paperCount) {
        this.paperCount = paperCount;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}
