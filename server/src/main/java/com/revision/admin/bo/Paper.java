package com.revision.admin.bo;

import java.sql.Date;

/**
 * Created by manodyas on 11/16/2017.
 */
public class Paper {
    private int paperId;
    private String subjectCode;
    private String description;
    private Date uploadedDate;
    private String username;
    private Date expiryDate;
    private String paperStatus;
    private String paperStatusDescription;
    private String paperDuration;

    public Paper() {
    }

    public Paper(int paperId, String subjectCode, String description, Date uploadedDate, String username, Date expiryDate,String paperStatus, String paperStatusDescription, String paperDuration) {
        this.paperId = paperId;
        this.subjectCode = subjectCode;
        this.description = description;
        this.uploadedDate = uploadedDate;
        this.username = username;
        this.expiryDate = expiryDate;
        this.paperStatus = paperStatus;
        this.paperStatusDescription = paperStatusDescription;
        this.paperDuration = paperDuration;
    }

    public Paper(int paperId) {
        this.paperId = paperId;
    }

    public Paper(String subjectCode, String description, Date uploadedDate, String username, Date expiryDate) {
        this.subjectCode = subjectCode;
        this.description = description;
        this.uploadedDate = uploadedDate;
        this.username = username;
        this.expiryDate = expiryDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getPaperId() {
        return paperId;
    }

    public void setPaperId(int paperId) {
        this.paperId = paperId;
    }

    public String getPaperStatus() {
        return paperStatus;
    }

    public void setPaperStatus(String paperStatus) {
        this.paperStatus = paperStatus;
    }

    public String getPaperStatusDescription() {
        return paperStatusDescription;
    }

    public void setPaperStatusDescription(String paperStatusDescription) {
        this.paperStatusDescription = paperStatusDescription;
    }

    public String getPaperDuration() {
        return paperDuration;
    }

    public void setPaperDuration(String paperDuration) {
        this.paperDuration = paperDuration;
    }
}
