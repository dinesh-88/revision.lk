package com.revision.admin.bo;

import java.sql.Date;

/**
 * Created by manodyas on 11/17/2017.
 */
public class PaperDetail {
    private String paperId;
    private String questionId;
    private String question;
    private String answer;
    private String sectionCode;
    private String subSectionCode;
    private String description;
    private String imageUrl;
    private String choise1;
    private String choise2;
    private String choise3;
    private String choise4;
    private String choise5;
    private Date uploadedDate;

    public PaperDetail() {
    }

    public PaperDetail(String paperId, String questionId, String question, String answer, String sectionCode, String subSectionCode, String description, String imageUrl, String choise1, String choise2, String choise3, String choise4, String choise5, Date uploadedDate) {
        this.paperId = paperId;
        this.questionId = questionId;
        this.question = question;
        this.answer = answer;
        this.sectionCode = sectionCode;
        this.subSectionCode = subSectionCode;
        this.description = description;
        this.imageUrl = imageUrl;
        this.choise1 = choise1;
        this.choise2 = choise2;
        this.choise3 = choise3;
        this.choise4 = choise4;
        this.choise5 = choise5;
        this.uploadedDate = uploadedDate;
    }

    public String getPaperId() {
        return paperId;
    }

    public void setPaperId(String paperId) {
        this.paperId = paperId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getSubSectionCode() {
        return subSectionCode;
    }

    public void setSubSectionCode(String subSectionCode) {
        this.subSectionCode = subSectionCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getChoise1() {
        return choise1;
    }

    public void setChoise1(String choise1) {
        this.choise1 = choise1;
    }

    public String getChoise2() {
        return choise2;
    }

    public void setChoise2(String choise2) {
        this.choise2 = choise2;
    }

    public String getChoise3() {
        return choise3;
    }

    public void setChoise3(String choise3) {
        this.choise3 = choise3;
    }

    public String getChoise4() {
        return choise4;
    }

    public void setChoise4(String choise4) {
        this.choise4 = choise4;
    }

    public String getChoise5() {
        return choise5;
    }

    public void setChoise5(String choise5) {
        this.choise5 = choise5;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }
}
