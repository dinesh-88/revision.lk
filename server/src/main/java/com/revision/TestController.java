package com.revision;

import com.google.gson.Gson;
import com.revision.admin.dao.MySqlDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;
import javax.validation.Valid;

/**
 * Created by manodyas on 11/14/2017.
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public  class TestController {
    private static final Logger logger = LogManager.getLogger(TestController.class);
    private static Gson gson = new Gson();

    @Autowired
    DataSource dataSource;

    @Autowired
    MySqlDaoImpl mySqlDao;

    @RequestMapping(value = "/test/", method = {RequestMethod.POST}/*,  headers = {"Content-type=application/json"}*/)
    @ResponseBody
    public String getUserInfo(@RequestBody @Valid String inquiryRequest) throws Exception {
        logger.info("Request:" + inquiryRequest);
       // MySqlDaoImpl mySqlDao = new MySqlDaoImpl();
        return gson.toJson(mySqlDao.findAllCustomers());
    }

}