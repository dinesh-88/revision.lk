CREATE DATABASE  IF NOT EXISTS `revision` DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `subject` (
  `id` bigint(10) NOT NULL,
  `subject_code` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_code_UNIQUE` (`subject_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `subject` (`id`,`subject_code`,`description`,`created_date`) VALUES (1,'01','Physics(භෞතික විද්‍යාව)','2017-11-14 23:19:14');
INSERT INTO `subject` (`id`,`subject_code`,`description`,`created_date`) VALUES (2,'02','Chemistry(රසායන විද්‍යාව)','2017-11-14 23:20:54');
INSERT INTO `subject` (`id`,`subject_code`,`description`,`created_date`) VALUES (3,'09','Biology(ජීව විද්‍යාව)','2017-11-14 23:25:34');

CREATE TABLE IF NOT EXISTS  `section` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(50) DEFAULT NULL,
  `section_code` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `section_code_UNIQUE` (`section_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (1,'01','01_1','Measurement(මිනුම)','2017-11-15 00:16:05');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (2,'01','01_2','Mechanics(යාන්ත්‍ර විද්‍යාව)','2017-11-15 00:19:23');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (3,'01','01_3','Oscillations and Waves(දෝලන සහ තරංග)','2017-11-15 00:24:52');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (4,'01','01_4','Thermal Physics(තාප භෞතිකය)','2017-11-15 00:26:18');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (5,'01','01_5','Gravitational Field(ගුරුත්වාකර්ෂණ ක්ෂේත්‍රය)','2017-11-15 00:44:34');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (6,'01','01_6','Electrostatic field(ස්ථිති විද්‍යුත් ක්ෂේත්‍රය)','2017-11-15 00:46:16');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (7,'01','01_7','Magnetic Field(චුම්භක ක්ෂේත්‍රය)','2017-11-15 00:48:38');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (8,'01','01_8','Current Electricity(ධාරා විද්‍යුතය)','2017-11-15 00:48:40');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (9,'01','01_9','Electronics(ඉලෙක්ට්‍රොනික විද්‍යාව)','2017-11-15 00:54:07');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (10,'01','01_10','Mechanical Properties of Matter(පදාර්ථයේ යාන්ත්‍රික ගුණ)','2017-11-15 00:57:14');
INSERT INTO `section` (`id`,`subject_code`,`section_code`,`description`,`created_date`) VALUES (11,'01','01_11','Matter and Radiation(පදාර්ථ සහ විකිරණ)','2017-11-15 01:00:10');




CREATE TABLE `paper` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `uploaded_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` datetime DEFAULT NULL,
  `paper_status` varchar(5) DEFAULT NULL,
  `paper_duration` varchar(100) DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `reviewed_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK` (`subject_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;



CREATE DEFINER=`root`@`localhost` TRIGGER `revision`.`paper_BEFORE_INSERT` BEFORE INSERT ON `paper` FOR EACH ROW
-- BEGIN
  set NEW.expiry_date = DATE_ADD(NOW() , INTERVAL 1000 DAY); 
-- END;



CREATE TABLE IF NOT EXISTS `subsection` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(50) DEFAULT NULL,
  `section_code` varchar(50) DEFAULT NULL,
  `subsection_code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK` (`subject_code`,`section_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*
-- Query: select * from subsection
LIMIT 0, 1000

-- Date: 2017-11-17 12:04
*/
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_1','Kinematics');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_2','Resultant of forces');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_3','Force and motion');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_4','Equilibrium');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_5','Work, energy and power');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_6','Rotational motion');
INSERT INTO `subsection` (`subject_code`,`section_code`,`subsection_code`,`description`) VALUES ('01','01_2','01_2_7','Fluid-dynamics');


CREATE TABLE `paper_details` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `paper_id` bigint(10) DEFAULT NULL,
  `question_id` int(10) DEFAULT NULL,
  `question` varchar(2000) DEFAULT NULL,
  `answer` varchar(2000) DEFAULT NULL,
  `section_code` varchar(100) DEFAULT NULL,
  `subsection_code` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `image_url` varchar(1000) DEFAULT NULL,
  `choise_1` varchar(500) DEFAULT NULL,
  `choise_2` varchar(500) DEFAULT NULL,
  `choise_3` varchar(500) DEFAULT NULL,
  `choise_4` varchar(500) DEFAULT NULL,
  `choise_5` varchar(500) DEFAULT NULL,
  `uploaded_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK` (`paper_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `packages` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) DEFAULT NULL,
  `paper_tokens` int(11) DEFAULT NULL,
  `paper_count` int(11) DEFAULT NULL,
  `cost` double(40,2) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `isDefault` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `packages` (`id`,`package_name`,`paper_tokens`,`paper_count`,`cost`,`duration`,`isDefault`) VALUES (1,'Basic',50,4,4000.00,1,0);
INSERT INTO `packages` (`id`,`package_name`,`paper_tokens`,`paper_count`,`cost`,`duration`,`isDefault`) VALUES (2,'Intro',10,2,0.00,1,1);



CREATE TABLE `user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_login_time` datetime DEFAULT NULL,
  `subscription_end_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `session_id` varchar(100) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `package_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UQ` (`user_name`,`email`,`mobile`,`session_id`),
  KEY `FK` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



CREATE DEFINER=`root`@`localhost` TRIGGER `revision`.`user_BEFORE_INSERT` BEFORE INSERT ON `user` FOR EACH ROW
--BEGIN
 set NEW.subscription_end_date = DATE_ADD(NOW() , INTERVAL 30 DAY);
--END


CREATE TABLE `admin_session` (
  `user_id` bigint(10) DEFAULT NULL,
  `session_id` varchar(150) DEFAULT NULL,
  `last_activity_time` datetime DEFAULT NULL,
  UNIQUE KEY `id_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
