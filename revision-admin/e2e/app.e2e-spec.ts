import { RevisionPage } from './app.po';

describe('revision App', () => {
  let page: RevisionPage;

  beforeEach(() => {
    page = new RevisionPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
