import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Constants } from './../app.constants';

@Injectable()
export class DataService {

  constructor(private http:  HttpClient) { }

  getRequest() {
    return this.http.get(Constants.REST_BASE_PATH);
  }
  postRequest(p) {
    p.header.messageID = Math.floor(Date.now() / 1000);
    p.header.ip = '127.0.0.1';
    p.header.userID = 'Mano';
      return this.http.post(Constants.REST_BASE_PATH, p ? p : {});
  }
  uploadFiles(p) {
    // p.header.messageID = Math.floor(Date.now() / 1000);
    // p.header.ip = '127.0.0.1';
    // p.header.userID = 'Mano';

    const req = new HttpRequest('POST', Constants.UPLOAD_BASE_PATH + '/1/2', p);

    return this.http.request(req);
     // return this.http.post(Constants.UPLOAD_BASE_PATH, p ? p : {});
  }
}
