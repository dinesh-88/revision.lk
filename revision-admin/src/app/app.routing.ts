import {RouterModule, Routes} from '@angular/router';
import {PapersComponent} from './papers/papers.component';
import {UsersComponent} from "./users/users.component";
import {AuthComponent} from "./auth/auth.component";

const APP_ROUTE: Routes = [
  {path: 'paper', component: PapersComponent },
  {path: 'users', component: UsersComponent },
  {path: 'login', component: AuthComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTE);
