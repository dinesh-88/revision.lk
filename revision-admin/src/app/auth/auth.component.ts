import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  email: string;
  password: string;
  constructor(private dataService: DataService) { }

  ngOnInit() {
  }
  login() {
    this.dataService.postRequest({header: {msgType: 4000, }, body: {password: this.password, email: this.email }})
      .subscribe((data: any) => {
        console.log(data);
        localStorage.setItem('sessionID', data.header.sessionID);
      }, error => {
        console.log(error);
      });
  }
}
