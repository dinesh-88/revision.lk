import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ControlValueAccessor} from '@angular/forms';
import {noop} from 'rxjs/util/noop';

@Component({
  selector: 'app-tiny-mce',
  templateUrl: './tiny-mce.component.html',
  styleUrls: ['./tiny-mce.component.css']
})
export class TinyMCEComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {
  @Input() elementId: String;
  @Output() onEditorKeyup = new EventEmitter<any>();
  private innerValue: any = '';
  editor;
  private onChangeCallback: (_: any) => void = noop;
  private onTouchedCallback: () => void = noop;

  ngAfterViewInit() {
    tinymce.init({
      selector: '#' + this.elementId,
      plugins: ['link', 'paste', 'table'],
      skin_url: 'assets/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyup.emit(content);
          this.innerValue = content;
        });
      },
    });
  }
  get answer(): any {
    return this.innerValue;
  };
  onBlur() {
    this.onTouchedCallback();
  }
  // set accessor including call the onchange callback
  set answer(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }
  writeValue(answer: any) {
    if (answer !== this.innerValue) {
      this.innerValue = answer;
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
}
