import {Component, OnInit, ViewChild} from '@angular/core';
import { DataService } from './../services/data.service';
import {MatDialog} from '@angular/material';
import {PapersModalComponent} from './papers-modal/papers-modal.component';

@Component({
  selector: 'app-papers',
  templateUrl: './papers.component.html',
  styleUrls: ['./papers.component.css']
})
export class PapersComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  tinymce: any;
  username = 'Mano';
  papers: Paper[] = [];
  paperList = [];
  sections: Sections [];
  answers:  Answers[] = [];
  answerList:  Answers[] = [];
  subsections:  Subsection[] = [];
  subjectCode: string;
  isChangeSubject: boolean;
  showSections: boolean;
  showAnswers: boolean;
  paperId: string;
  description: string;
  paperDuration: number;
  constructor(private dataService: DataService, private dialog: MatDialog) { }

  ngOnInit() {
    this.showSections = false;
    this.showAnswers = false;
    this.dataService.postRequest({header: {msgType: 1003, statusCode: 0}, body: {paperId: 0, username: this.username }})
      .subscribe((data: any) => {
        this.paperList = data.body;
      }, error => {
        console.log(error);
      });
    this.dataService.postRequest({header: {msgType: 1000}})
      .subscribe((data: any) => {
          this.papers = data.body;
    }, error => {
        console.log(error);
        const test: Paper = { id: -1, subjectCode: '-1', description: 'No Subject found', createdDate: ''};
        this.papers.push(test);
      });
  }
  upload() {
    const fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      const formData = new FormData();
      formData.append('file', fileBrowser.files[0]);
      this.dataService.uploadFiles(formData).subscribe(res => {
        // do stuff w/my uploaded file
        console.log(res);
      });
    }
  }
  changeSubject() {
    this.dataService.postRequest({header: {msgType: 1001}, body: {subjectCode: this.subjectCode}})
      .subscribe((data: any) => {
        this.sections = data.body;
        this.isChangeSubject = true;
        this.description = '';
        this.paperDuration = 0;
      }, error => {
        console.log(error);
      });
  }
  clickAddAnswers() {
    // 1. create paper meta 2000
    this.dataService.postRequest({header: {msgType: 2000, statusCode: 0},
      body: {subjectCode: this.subjectCode, description: this.description, username: this.username, paperDuration: this.paperDuration}})
      .subscribe((data: any) => {
        this.showAnswers = true;
        this.paperId = data.body.paperId;
        const answer: Answers = {  question: '', description: '',
          choise1: '',
          sectionCode: '',
          choise2: '',
          choise3: '',
          choise4: '',
          choise5: '',
          correctAnswer: 0,
          image: '',
          isSaved: false,
          subsectionCode: ''};
        this.answers.push(answer)
      }, error => {
        console.log(error);
      });
  }
  addAnswer(i) {
    this.answers[i].isSaved = true;
    // 1. insert paper details 2001
    const answerRequest = {header: {msgType: 2001, statusCode: 0}, body: {paperId: this.paperId, questionId: i + 1, question: this.answers[i].question , answer: this.answers[i].correctAnswer, sectionCode: this.answers[i].sectionCode, subSectionCode: this.answers[i].subsectionCode , description: this.answers[i].description, choise1: this.answers[i].choise1, choise2: this.answers[i].choise2, choise3: this.answers[i].choise3, choise4: this.answers[i].choise4, choise5: this.answers[i].choise5
    }};
    this.dataService.postRequest(answerRequest)
      .subscribe((data: any) => {
        const answer: Answers = {   question: '', description: '',
          choise1: '',
          sectionCode: '',
          choise2: '',
          choise3: '',
          choise4: '',
          choise5: '',
          correctAnswer: 0,
          image: '',
          isSaved: false, subsectionCode: ''};
        this.answers.push(answer);
      }, error => {
        console.log(error);
      });

  }
  changeSection(answer) {
    this.dataService.postRequest({header: {msgType: 1002, statusCode: 0},
      body: {sectionCode: answer.sectionCode}})
      .subscribe((data: any) => {
        this.subsections = data.body;
      }, error => {
        console.log(error);
      });
  }

  clickPaper(paper) {
    // 1. get paper details 1004
    this.dataService.postRequest({header: {msgType: 1004, statusCode: 0},
      body: {paperId: paper.paperId}})
      .subscribe((data: any) => {
        paper.showAnswers = true;
        paper.answerList = data.body;
      }, error => {
        console.log(error);
      });
  }

  savePaper() {
    this.dataService.postRequest({header: {msgType: 3000, statusCode: 0},
      body: {paperId: this.paperId}})
      .subscribe((data: any) => {
     console.log(data);
      }, error => {
        console.log(error);
      });
  }
  openAddPaper() {
    this.dialog.open(PapersModalComponent, {width: '900px'})
  }
  clickReview(paperId) {
    this.dataService.postRequest({header: {msgType: 3001, statusCode: 0},
      body: {paperId: paperId}})
      .subscribe((data: any) => {
        console.log(data);
      }, error => {
        console.log(error);
      });
  }
}

interface Paper {
  id: number,
  subjectCode: string,
  description: string,
  createdDate: any
}

interface Sections {
  createdDate: any,
  description: string,
  sectionCode: string,
  subjectCode: string
}

interface Answers {
  question: string,
  description: string,
  sectionCode: string,
  choise1: string,
  choise2: string,
  choise3: string,
  choise4: string,
  choise5: string,
  correctAnswer: number,
  image: any,
  isSaved: boolean,
  subsectionCode: string
}

interface Subsection {
  createdDate: any,
  description: string,
  sectionCode: string,
  subjectCode: string,
  subsectionCode: string
}
