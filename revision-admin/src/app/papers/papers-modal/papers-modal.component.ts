import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from 'app/services/data.service';

@Component({
  selector: 'app-papers-modal',
  templateUrl: './papers-modal.component.html',
  styleUrls: ['./papers-modal.component.scss']
})
export class PapersModalComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  tinymce: any;
  username = 'Mano';
  papers: Paper[] = [];
  paperList = [];
  sections: Sections [];
  answers:  Answers[] = [];
  answerList:  Answers[] = [];
  subsections:  Subsection[] = [];
  subjectCode: string;
  isChangeSubject: boolean;
  showSections: boolean;
  showAnswers: boolean;
  paperId: string;
  description: string;
  paperDuration: number;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.showSections = false;
    this.showAnswers = false;
    this.dataService.postRequest({header: {msgType: 1003, statusCode: 0}, body: {paperId: 0, username: this.username }})
      .subscribe((data: any) => {
        this.paperList = data.body;
      }, error => {
        console.log(error);
      });
    this.dataService.postRequest({header: {msgType: 1000}})
      .subscribe((data: any) => {
        this.papers = data.body;
      }, error => {
        console.log(error);
        const test: Paper = { id: -1, subjectCode: '-1', description: 'No Subject found', createdDate: ''};
        this.papers.push(test);
      });
  }
  changeSubject() {
    this.dataService.postRequest({header: {msgType: 1001}, body: {subjectCode: this.subjectCode}})
      .subscribe((data: any) => {
        this.sections = data.body;
        this.isChangeSubject = true;
        this.description = '';
        this.paperDuration = 0;
      }, error => {
        console.log(error);
      });
  }
}

interface Paper {
  id: number,
  subjectCode: string,
  description: string,
  createdDate: any
}

interface Sections {
  createdDate: any,
  description: string,
  sectionCode: string,
  subjectCode: string
}

interface Answers {
  question: string,
  description: string,
  sectionCode: string,
  choise1: string,
  choise2: string,
  choise3: string,
  choise4: string,
  choise5: string,
  correctAnswer: number,
  image: any,
  isSaved: boolean,
  subsectionCode: string
}

interface Subsection {
  createdDate: any,
  description: string,
  sectionCode: string,
  subjectCode: string,
  subsectionCode: string
}
