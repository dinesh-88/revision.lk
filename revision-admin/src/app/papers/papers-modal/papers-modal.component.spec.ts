import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PapersModalComponent } from './papers-modal.component';

describe('PapersModalComponent', () => {
  let component: PapersModalComponent;
  let fixture: ComponentFixture<PapersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PapersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PapersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
