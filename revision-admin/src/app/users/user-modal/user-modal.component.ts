import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {
  userName: string;
  password: string;
  email: string;
  mobile: string;
  firstName: string;
  lastName: string;
  status: string;
  roleId: string;
  packageId: string;
  constructor(private dataService: DataService) { }

  ngOnInit() {
  }
  clickSave() {
    this.dataService.postRequest({header: {msgType: 2002, statusCode: 0},
      body: {userName: this.userName, password: this.password , email: this.email, mobile: this.mobile,
        firstName: this.firstName, lastName: this.lastName, status: this.status, roleId: this.roleId, packageId: this.packageId}})
      .subscribe((data: any) => {
      console.log(data);
      }, error => {
        console.log(error);
      });
  }
}
