import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import {UserModalComponent} from './user-modal/user-modal.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {

  }
  openAddUser() {
    this.dialog.open(UserModalComponent, {width: '900px'})
  }

}
