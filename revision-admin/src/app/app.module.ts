import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PapersComponent } from './papers/papers.component';

import { DataService } from './services/data.service';
import { SidebarComponent } from './sidebar.component'
import { routing } from './app.routing';
import { TinyMCEComponent } from './tiny-mce/tiny-mce.component';
import { UsersComponent } from './users/users.component';
import { UserModalComponent } from './users/user-modal/user-modal.component';
import { PapersModalComponent } from './papers/papers-modal/papers-modal.component';
import { AuthComponent } from './auth/auth.component';

@NgModule({
  declarations: [
    AppComponent,
    PapersComponent,
    SidebarComponent,
    TinyMCEComponent,
    UsersComponent,
    UserModalComponent,
    PapersModalComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  entryComponents: [UserModalComponent,PapersModalComponent],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
